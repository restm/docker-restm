# Restm Platform

> **Purpose:** Obtain quality real estate (purchase and rent) metrics


## Setup
### Dependencies

+ [Ahoy](https://github.com/ahoy-cli/ahoy)
+ [Docker](https://www.docker.com/)

### Build containers
```
ahoy docker build
```

### Run containers

```
ahoy docker up
ahoy setup
```

### Scrap

```
ahoy scrap
```


## TODO's

- Tests for everything
- Extends spiders to additional Buenos Aires data sources
- Add New York

