import os
from grafana_api_client import GrafanaClient

BASE_DIR = os.path.dirname(os.path.realpath(__file__))


GRAFANA = {
    'host': os.environ.get('GRAFANA_HOST', '0.0.0.0'),
    'user': os.environ.get('GRAFANA_USER', 'admin'),
    'password': os.environ.get('GF_SECURITY_ADMIN_PASSWORD', 'admin'),
    'port': os.environ.get('GRAFANA_PORT', 3005)
}

DATA_SOURCES = [
    {
        'name': 'influxdb',
        'type': 'influxdb',
        'access': 'proxy',
        'url': 'http://%s:8086' % os.environ.get('INFLUXDB_HOST', 'influxdb'),
        'password': os.environ.get('INFLUXDB_USER_PASSWORD', 'restm'),
        'user': os.environ.get('INFLUXDB_USER', 'restm'),
        'database': os.environ.get('INFLUXDB_DB', 'restm'),
        'basicAuth': False,
        'isDefault': False,
    }
]

GRAFANA_CLIENT = GrafanaClient((GRAFANA['user'], GRAFANA['password']),
                               host=GRAFANA['host'], port=GRAFANA['port'])
